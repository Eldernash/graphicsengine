#version 410

in vec3 vNormal;
in vec2 vTexCoord;
in vec3 vTangent;
in vec3 vBiTangent;
in vec4 vPosition;

uniform sampler2D DiffuseTexture;
uniform sampler2D SpecularTexture;
uniform sampler2D NormalTexture;

uniform vec3 CameraPosition;
uniform vec3 Ia; // ambient light colour
uniform vec3 Id[2]; // diffuse light colour
uniform vec3 Is[2]; // specular light colour
uniform vec3 LightDirection[2];

uniform vec3 Ka; // ambient material colour
uniform vec3 Kd; // diffuse material colour
uniform vec3 Ks; // specular material colour
uniform float specularPower; // material specular power

out vec4 FragColour;

void main() {
	vec3 N = normalize(vNormal);
	vec3 T = normalize(vTangent);
	vec3 B = normalize(vBiTangent);
	
	vec3 texDiffuse = texture( DiffuseTexture, vTexCoord ).rgb;
	vec3 texSpecular = texture( SpecularTexture, vTexCoord ).rgb;
	vec3 texNormal = texture( NormalTexture, vTexCoord ).rgb;

	vec3 ambient = Ia * Ka;
	FragColour = vec4(ambient, 1);
	// calculate each colour property
	mat3 TBN = mat3(T, B, N);
	N = normalize(TBN * (texNormal * 2 - 1));
		vec3 V = normalize(CameraPosition - vPosition.xyz);

	vec4 spec0;
	vec3 diffuse0;
	for(int i = 0; i < 2; i++) {
		vec3 L = normalize(LightDirection[i]);
	
		// calculate lambert term (negative light direction)
		float lambertTerm = max(0, min( 1, dot(N, -L)));

		// calculate view vector and reflection vector
		vec3 R = reflect( L, N );

		// calculate specular term
		float specularTerm = pow( max( 0, dot( R, V ) ), specularPower );

		vec3 diffuse = Id[i] * Kd * texDiffuse * lambertTerm;
		vec3 specular = Is[i] * Ks * texSpecular * specularTerm;

		spec0 = spec0 + vec4(specular * texSpecular, 1);
		diffuse0 = diffuse0 + diffuse;
	}
	
	FragColour = FragColour + spec0 + vec4( (ambient + diffuse0) * texDiffuse, 0 );
}