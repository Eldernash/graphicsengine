#pragma once

#include "Application.h"
#include "Mesh.h"
#include "OBJMesh.h"
#include "Camera.h"
#include "Shader.h"
#include "Texture.h"
#include "Light.h"
#include <imgui.h>

class Application3D : public aie::Application {
public:

	Application3D();
	virtual ~Application3D();

	virtual bool startup();
	virtual void shutdown();

	virtual void update(float deltaTime);
	virtual void draw();

protected:
	Camera	*	m_camera;

	glm::mat4	m_viewMatrix;
	glm::mat4	m_projectionMatrix;

	aie::ShaderProgram m_spearShader;
	aie::OBJMesh m_spearMesh;

	aie::ShaderProgram m_bunnyShader;
	aie::OBJMesh m_bunnyMesh;

	aie::ShaderProgram m_buddhaShader;
	aie::OBJMesh m_buddhaMesh;
	
	aie::ShaderProgram m_dragonShader;
	aie::OBJMesh m_dragonMesh;

	aie::ShaderProgram m_lucyShader;
	aie::OBJMesh m_lucyMesh;

	// the array for the meshes - array size should be set to the number of meshes to be loaded onto the scene
	int entitiyCount;
	Mesh	*	m_meshes[5];

	Light		m_lights[2];
	glm::vec3	m_ambientLight;
};