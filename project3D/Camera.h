#pragma once
#include <glm/glm.hpp>
#include <glm/ext.hpp>
#include "Input.h"
class Camera {
public:
	Camera() : theta(0), phi(-20), position(-10, 4, 0) {}
	~Camera();
	glm::mat4 GetProjectionMatrix(float w, float h);
	glm::mat4 GetViewMatrix();
	void Update();

private:
	int lastMouseX;
	int lastMouseY;
	float theta;
	float phi;
	glm::vec3 position;
};
