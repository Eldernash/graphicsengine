#pragma once
#include <glm/mat4x4.hpp>
class Light {
public:
	Light();
	~Light();

	glm::vec3 direction;
	glm::vec3 diffuse;
	glm::vec3 specular;
};

