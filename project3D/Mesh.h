#pragma once
#include <glm\glm.hpp>
#include "Shader.h"
#include "OBJMesh.h"
#include <glm/mat4x4.hpp>
#include <glm/ext.hpp>
#include <string.h>
#include "Camera.h"
#include "Light.h"
class Mesh {
public:
	Mesh() : triCount(0), vao(0), vbo(0), ibo(0) {}
	Mesh(aie::OBJMesh * mesh, aie::ShaderProgram *shader, float x, float y, float z, float scale) { Initialise(mesh, shader, x, y, z, scale); }

	virtual ~Mesh();

	struct Vertex {
		glm::vec4 position;
		glm::vec4 normal;
		glm::vec2 texCoord;
	};

	// loads the vertex and fragment shaders for the mesh
	void LoadShaders(aie::ShaderProgram *shader);

	// loads meshe objects
	void LoadMesh(aie::OBJMesh * mesh);

	void InitialiseQuad();

	void Initialise(aie::OBJMesh * mesh, aie::ShaderProgram *shader, float x, float y, float z, float scale);

	virtual void Draw(Light* lights, glm::mat4 projectionMatrix, glm::mat4 viewMatrix, glm::vec3 ambientLight);

private:
	// mesh
	unsigned int triCount;
	unsigned int vao, vbo, ibo;

	aie::ShaderProgram * m_shader;
	aie::OBJMesh * m_mesh;
	glm::mat4 m_transform;
};

