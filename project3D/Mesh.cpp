#include "Mesh.h"
#include <gl_core_4_4.h>

Mesh::~Mesh() {
	glDeleteVertexArrays(1, &vao);
	glDeleteBuffers(1, &vbo);
	glDeleteBuffers(1, &ibo);
}

void Mesh::LoadShaders(aie::ShaderProgram *shader) {
	m_shader = shader;
}
void Mesh::LoadMesh(aie::OBJMesh * mesh) {
	m_mesh = mesh;
}

void Mesh::InitialiseQuad() {
	// check that the mesh is not initialised already
	assert(vao == 0);

	// generate buffers
	glGenBuffers(1, &vbo);
	glGenVertexArrays(1, &vao);

	// bind vertex array aka a mesh wrapper
	glBindVertexArray(vao);

	// bind vertex buffer
	glBindBuffer(GL_ARRAY_BUFFER, vbo);

	// define six vertices for two triangles
	Vertex vertices[6];
	vertices[0].position = { -0.5f, 0, 0.5f, 1 };
	vertices[1].position = { 0.5f, 0, 0.5f, 1 };
	vertices[2].position = { -0.5f, 0, -0.5f, 1 };

	vertices[3].position = { -0.5f, 0, -0.5f, 1 };
	vertices[4].position = { 0.5f, 0, 0.5f, 1 };
	vertices[5].position = { 0.5f, 0, -0.5f, 1 };

	// defines the normal direction for the vertices
	vertices[0].normal = { 0,1,0,0 };
	vertices[1].normal = { 0,1,0,0 };
	vertices[2].normal = { 0,1,0,0 };

	vertices[3].normal = { 0,1,0,0 };
	vertices[4].normal = { 0,1,0,0 };
	vertices[5].normal = { 0,1,0,0 };

	// defines texture coordinates for the quad
	vertices[0].texCoord = { 0, 1 };
	vertices[1].texCoord = { 1, 1 };
	vertices[2].texCoord = { 0, 0 };

	vertices[3].texCoord = { 0, 0 };
	vertices[4].texCoord = { 1, 1 };
	vertices[5].texCoord = { 1, 0 };

	// fill vertex buffer
	glBufferData(GL_ARRAY_BUFFER, 6 * sizeof(Vertex), vertices, GL_STATIC_DRAW);

	// enable first element as position
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0,4,GL_FLOAT,GL_FALSE,sizeof(Vertex), 0);

	// enable third element as texture
	glEnableVertexAttribArray(2);
	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (void*)32);

	// unbind buffers
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	// quad has two triangles
	triCount = 2;
}

void Mesh::Initialise(aie::OBJMesh * mesh, aie::ShaderProgram *shader ,float x, float y, float z, float s) {

	m_transform = { s,0,0,0,
					0,s,0,0,
					0,0,s,0,
					x,y,z,s };
	m_shader = shader;
	m_mesh = mesh;
}

void Mesh::Draw(Light* lights, glm::mat4 projectionMatrix, glm::mat4 viewMatrix, glm::vec3 ambientLight) {

	const int arraySize = 2;

	// Adds the lights' direction, colour and specular to respectice arrays
	glm::vec3 lDiffuse[arraySize];
	for (int i = 0; i < arraySize; i++) {
		lDiffuse[i] = lights[i].diffuse;
	}
	glm::vec3 lSpecular[arraySize];
	for (int i = 0; i < arraySize; i++) {
		lSpecular[i] = lights[i].specular;
	}
	glm::vec3 lDirection[arraySize];
	for (int i = 0; i < arraySize; i++) {
		lDirection[i] = lights[i].direction;
	}

	// bind shader
	m_shader->bind();

	// bind light
	m_shader->bindUniform("Ia", ambientLight);
	m_shader->bindUniform("Id", 2, lDiffuse);
	m_shader->bindUniform("Is", 2, lSpecular);
	m_shader->bindUniform("LightDirection", 2, lDirection);

	// bind transform
	auto pvm = projectionMatrix * viewMatrix * m_transform;
	m_shader->bindUniform("ProjectionViewModel", pvm);

	// bind camera position
	m_shader->bindUniform("CameraPosition", glm::vec3(glm::inverse(viewMatrix)[3]));

	// bind transforms for lighting
	m_shader->bindUniform("NormalMatrix", glm::inverseTranspose(glm::mat3(m_transform)));

	// bind texture locations
	m_shader->bindUniform("DiffuseTexture", 0);
	m_shader->bindUniform("SpecularTexture", 0);
	m_shader->bindUniform("NormalTexture", 0);

	// draw mesh
	m_mesh->draw();

}
