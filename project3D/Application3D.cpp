#include "Application3D.h"
#include "Gizmos.h"

using glm::vec3;
using glm::vec4;
using glm::mat4;
using aie::Gizmos;

Application3D::Application3D() { }

Application3D::~Application3D() { }

bool Application3D::startup() {
	
	entitiyCount = 5;

	m_camera = new Camera;

	setBackgroundColour(0.25f, 0.25f, 0.25f);

	// initialise gizmo primitive counts
	Gizmos::create(10000, 10000, 10000, 10000);

	// create simple camera transforms
	m_viewMatrix = glm::lookAt(vec3(10), vec3(0), vec3(0, 1, 0));
	m_projectionMatrix = glm::perspective(glm::pi<float>() * 0.25f, getWindowWidth() / (float)getWindowHeight(), 0.1f, 1000.f);

	// loads vertex and fragment shaders from file
	m_spearShader.loadShader(aie::eShaderStage::VERTEX, "./shaders/phongNormal.vert");
	m_spearShader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/phongNormal.frag");
	if (m_spearShader.link() == false) {
		printf("Shader Error: %s\n", m_spearShader.getLastError());
	}
	m_bunnyShader.loadShader(aie::eShaderStage::VERTEX, "./shaders/rimLight.vert");
	m_bunnyShader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/rimLight.frag");
	if (m_bunnyShader.link() == false) {
		printf("Shader Error: %s\n", m_bunnyShader.getLastError());
	}
	m_buddhaShader.loadShader(aie::eShaderStage::VERTEX, "./shaders/phong.vert");
	m_buddhaShader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/phong.frag");
	if (m_buddhaShader.link() == false) {
		printf("Shader Error: %s\n", m_buddhaShader.getLastError());
	}
	m_dragonShader.loadShader(aie::eShaderStage::VERTEX, "./shaders/phong.vert");
	m_dragonShader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/phong.frag");
	if (m_dragonShader.link() == false) {
		printf("Shader Error: %s\n", m_dragonShader.getLastError());
	}
	m_lucyShader.loadShader(aie::eShaderStage::VERTEX, "./shaders/phong.vert");
	m_lucyShader.loadShader(aie::eShaderStage::FRAGMENT, "./shaders/phong.frag");
	if (m_lucyShader.link() == false) {
		printf("Shader Error: %s\n", m_lucyShader.getLastError());
	}
	
	// loads the mesh data from file
	if (m_spearMesh.load("./soulspear/soulspear.obj", true, true) == false) {
		printf("Mesh error!\n");
	}
	if (m_bunnyMesh.load("./stanford/Bunny.obj", true, true) == false) {
		printf("Mesh error!\n");
	}
	if (m_buddhaMesh.load("./stanford/Buddha.obj", true, true) == false) {
		printf("Mesh error!\n");
	}
	if (m_dragonMesh.load("./stanford/Dragon.obj", true, true) == false) {
		printf("Mesh error!\n");
	}
	if (m_lucyMesh.load("./stanford/Lucy.obj", true, true) == false) {
		printf("Mesh error!\n");
	}

	for (int i = 0; i < 2; i++) {
		m_lights[i].direction = { 1 ,1, 1 };
		m_lights[i].diffuse = { 1, 1, 0 };
		m_lights[i].specular = { 1, 1, 0 };
	}

	m_ambientLight = { 0.1f, 0.1f, 0.1f };

	// initalisation requirements for meshes
	m_meshes[0] = new Mesh(&m_spearMesh, &m_spearShader, 0, 0, 0, 1);
	m_meshes[1] = new Mesh(&m_bunnyMesh, &m_bunnyShader, 10, 0, 0, 1);
	m_meshes[2] = new Mesh(&m_buddhaMesh, &m_buddhaShader, -10, 0, 0, 1);
	m_meshes[3] = new Mesh(&m_dragonMesh, &m_dragonShader, 0, 0, 10, 1);
	m_meshes[4] = new Mesh(&m_lucyMesh, &m_lucyShader, 0, 0, -10, 1);
	return true;
}

void Application3D::shutdown() {
	Gizmos::destroy();
}

void Application3D::update(float deltaTime) {

	// query time since application started
	float time = getTime();

	m_camera->Update();

	// wipe the gizmos clean for this frame
	Gizmos::clear();

	// draws a simple grid with gizmos
	vec4 white(1);
	vec4 black(0, 0, 0, 1);
	for (int i = 0; i < 21; ++i) {
		Gizmos::addLine(vec3(-10 + i, 0, 10), vec3(-10 + i, 0, -10), i == 10 ? white : black);
		Gizmos::addLine(vec3(10, 0, -10 + i), vec3(-10, 0, -10 + i), i == 10 ? white : black);
	}

	// add a transform so that we can see the axis
	Gizmos::addTransform(mat4(1));

	// quit if we press escape
	aie::Input* input = aie::Input::getInstance();

	if (input->isKeyDown(aie::INPUT_KEY_ESCAPE)) quit();
}

void Application3D::draw() {

	// wipe the screen to the background colour
	clearScreen();

	// light manipulation
	ImGui::Begin("Light 1");
	ImGui::SliderFloat("x", &m_lights[0].direction.x, -180, 180);
	ImGui::SliderFloat("Y", &m_lights[0].direction.y, -180, 180);
	ImGui::SliderFloat("Z", &m_lights[0].direction.z, -180, 180);
	ImGui::SliderFloat("DR", &m_lights[0].diffuse.r, 0, 1);
	ImGui::SliderFloat("DG", &m_lights[0].diffuse.g, 0, 1);
	ImGui::SliderFloat("DB", &m_lights[0].diffuse.b, 0, 1);
	ImGui::End();

	ImGui::Begin("Light 2");
	ImGui::SliderFloat("x", &m_lights[1].direction.x, -180, 180);
	ImGui::SliderFloat("Y", &m_lights[1].direction.y, -180, 180);
	ImGui::SliderFloat("Z", &m_lights[1].direction.z, -180, 180);
	ImGui::SliderFloat("DR", &m_lights[1].diffuse.r, 0, 1);
	ImGui::SliderFloat("DG", &m_lights[1].diffuse.g, 0, 1);
	ImGui::SliderFloat("DB", &m_lights[1].diffuse.b, 0, 1);
	ImGui::End();

	ImGui::Begin("Ambient Light");
	ImGui::SliderFloat("R", &m_ambientLight.r, 0, 1);
	ImGui::SliderFloat("G", &m_ambientLight.g, 0, 1);
	ImGui::SliderFloat("B", &m_ambientLight.b, 0, 1);
	ImGui::End();
	
	// update perspective in case window resized
	m_projectionMatrix = m_camera->GetProjectionMatrix(getWindowWidth(), getWindowHeight());
	m_viewMatrix = m_camera->GetViewMatrix();

	// Draws all meshes in the mesh list
	for (int i = 0; i < entitiyCount; i++)
		m_meshes[i]->Draw(&m_lights[0], m_projectionMatrix, m_viewMatrix, m_ambientLight);

	// draw 3D gizmos
	Gizmos::draw(m_projectionMatrix * m_viewMatrix);

	// draw 2D gizmos using an orthogonal projection matrix
	Gizmos::draw2D((float)getWindowWidth(), (float)getWindowHeight());

}